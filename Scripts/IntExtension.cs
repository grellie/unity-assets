﻿using UnityEngine;
using System.Collections;

public static class IntExtension
{
	public static int Loop(this int value, int min, int max)
	{
		if(value > max)
		{
			int cachedMin = min;
			min -= cachedMin; //it is 0 now
			max -= cachedMin;
			value -= cachedMin;
			value -= ((int)(value / max)) * max;

			min += cachedMin; //it is 0 now
			max += cachedMin;
			value += cachedMin;
		}
		if(value < min)
		{
			int cachedMax = max;
			min = -min + cachedMax; //it is 0 now
			max = -max + cachedMax;
			value = -value + cachedMax;

			value -= ((int)(value / min)) * min;

			min = -min + cachedMax; //it is 0 now
			max = -max + cachedMax;
			value = -value + cachedMax;
		}
		return value;
	} 

}

