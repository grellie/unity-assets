﻿using UnityEngine;
using System.Collections;

public static class FloatExtension
{
	public static float Loop(this float value, float min, float max)
	{
		if(value > max)
		{
			float cachedMin = min;
			min -= cachedMin; //it is 0 now
			max -= cachedMin;
			value -= cachedMin;
			value -= ((int)(value / max)) * max;

			min += cachedMin; //it is 0 now
			max += cachedMin;
			value += cachedMin;
		}
		if(value < min)
		{
			float cachedMax = max;
			min = -min + cachedMax; //it is 0 now
			max = -max + cachedMax;
			value = -value + cachedMax;

			value -= ((int)(value / min)) * min;

			min = -min + cachedMax; //it is 0 now
			max = -max + cachedMax;
			value = -value + cachedMax;
		}
		return value;
	} 

}

