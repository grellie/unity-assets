﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace UnityEngine.UI.Components
{
	[AddComponentMenu("Layout/Bubble Layout Group")]
	[SelectionBase]
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]

	public class BubbleLayoutGroup : UIBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
	{
		#region parameters
		[SerializeField] Vector2 cellSize = new Vector2(100,100);
		[SerializeField] constraint Constraint;
		[SerializeField] int constraintCount = 2;
		[SerializeField] Vector2 spacing = Vector2.zero;
		[SerializeField] bool inertia;
		[SerializeField] float decelerationRate = 0.135f;
		[SerializeField] AnimationCurve curve = AnimationCurve.Linear(0f, 1f, 1f,1f);
		#endregion

		#region private parameters
		RectTransform rect;
		int countX;
		int countY;
		float halfHeight;
		float halfWidth;
		float visibleHalfWidth;
		float visibleHalfHeight;
		float height;
		float width;
		float sizeX;
		float sizeY;
		float startX;
		float startY;

	 	bool isDraging = false;
		bool isInertia = false;

		Vector2 delta = Vector2.zero;
		Vector2 startDragPosition = Vector2.zero;
		Vector2 moveVelocity = Vector2.zero;

		Dictionary<RectTransform, BubbleUI> dict = new Dictionary<RectTransform, BubbleUI>();
		List<RectTransform> list = new List<RectTransform>();
		#endregion

		#region UIBehaviour
		protected override void OnRectTransformDimensionsChange ()
		{
			base.OnRectTransformDimensionsChange ();
			UpdateSizes();
			UpdatePositions();
		}
		protected override void Start ()
		{
			base.Awake ();
			Rebuild();
		}
		void LateUpdate()
		{
			if(isInertia)
			{
				moveVelocity *= Mathf.Pow(decelerationRate, Time.fixedDeltaTime);
				delta += moveVelocity;
				delta.x = delta.x.Loop(-halfWidth, halfWidth);
				delta.y = delta.y.Loop(-halfHeight, halfHeight);
				if(moveVelocity.magnitude < 1)
					isInertia = isDraging = false;

			}
			if(isDraging) 
			{
				startX = sizeX/2f - sizeX * countX / 2f + delta.x;
				startY = sizeY/2f - sizeY * countY / 2f + delta.y;
				UpdatePositions();
			}
		}
		#endregion

		public void Rebuild () 
		{
			CreateList();
			UpdateSizes();
			FillDictionary();
			UpdatePositions();
		}

		#region update functions
		void CreateList()
		{
			if(rect == null) rect = gameObject.GetComponent<RectTransform>();
			list = new List<RectTransform>(gameObject.GetComponentsInChildren<RectTransform>());
			list.Remove(rect);
		}
		void FillDictionary()
		{
			dict.Clear();
			bool shift = true;
			int id = 0;

			for(int y = 0; y < countY; y++)
			{
				for(int x = 0; x < countX; x++) 
				{
					if(list.Count > id)
					{
						dict.Add(list[id], new BubbleUI(x, y, shift));
						id++;		
					}
					else break;
				}
				shift = !shift;
			}
		}
		void UpdateSizes()
		{
			if(rect == null) rect = gameObject.GetComponent<RectTransform>();

			sizeX = cellSize.x + spacing.x;
			sizeY = cellSize.y + spacing.y;
			switch(Constraint)
			{
				case constraint.FixedColumnCount: 
				countX = constraintCount;
				countY = (list.Count + countX - 1) / countX;
				break;

			case constraint.FixedRowCount: 
				countY = constraintCount;
				countX = (list.Count + countY - 1) / countY;
				break;
			}
			width = sizeX * countX;
			height = sizeY * countY;

			visibleHalfWidth = rect.rect.width / 2f;
			visibleHalfHeight = rect.rect.height / 2f;

			halfWidth = width / 2f;
			halfHeight = height / 2f;

			startX = sizeX/2f - sizeX * countX / 2f;
			startY = sizeY/2f - sizeY * countY / 2f;
		}
		void UpdatePositions()
		{
			foreach(KeyValuePair<RectTransform, BubbleUI> pair in dict)
			{
				RectTransform rt =  pair.Key;
				BubbleUI bubble = pair.Value;

				Vector3 position = new Vector3(startX + bubble.x * sizeX - (bubble.shift? sizeX/2f : 0f ) , startY + bubble.y * sizeY , 0);

				position.x = position.x.Loop(-halfWidth, halfWidth);
				position.y = position.y.Loop(-halfHeight, halfHeight);

				rt.localPosition = position;
				float size = Mathf.Max(Mathf.Abs(position.x / visibleHalfWidth), //(2f *
						Mathf.Abs(position.y / visibleHalfHeight)); //(2f*
				size = Mathf.Min(size, 1f);
				rt.sizeDelta = 
					curve.Evaluate(size) * new Vector2(sizeX - spacing.x, sizeY - spacing.y );

			}
		}
		#endregion

		#region IHandler implementation
		public void OnDrag (PointerEventData data)
		{
			moveVelocity = data.position - startDragPosition;
			delta += moveVelocity;
			startDragPosition = data.position;
			delta.x = delta.x.Loop(-halfWidth, halfWidth);
			delta.y = delta.y.Loop(-halfHeight, halfHeight);
		}
		public void OnEndDrag (PointerEventData data)
		{
			if(inertia)  isInertia = true;
			else isDraging = false;
		}
		public void OnBeginDrag (PointerEventData data)
		{
			isDraging = true;
			isInertia = false;
			startDragPosition = data.position;
		}
		#endregion

		[Serializable] 
		class BubbleUI 
		{
			public int x;
			public int y;
			public bool shift;

			public BubbleUI(int x, int y, bool shift)
			{
				this.x = x;
				this.y = y;
				this.shift = shift;
			}
			public BubbleUI()
			{
				this.x = 0;
				this.y = 0;
				this.shift = false;
			}
		}

		enum constraint 
		{ 
			FixedColumnCount, FixedRowCount 
		}
	}
}
