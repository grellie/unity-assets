﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Components
{
	public enum CircleLayoutRotationType
	{
		LocalRotation, GlobalRotation, CenterRotation, CenterRotationInvertedLocal, CenterRotationInvertedGlobal
	}

	public enum CircleLayoutState { Showen, Hidden, ShowenToHidden, HiddenToShowen, None}

	[AddComponentMenu("Layout/Animated Circle Layout Group")]
	[SelectionBase]
	[ExecuteInEditMode]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	public class CircleLayoutGroup : UIBehaviour
	{
		#region parameters
		[Header("Main")]
		[SerializeField] CircleLayoutState startState = CircleLayoutState.Showen;

		RectTransform rect;
		List<RectTransform> list = new List<RectTransform>();

		[Header("Start degree")]
		[SerializeField] float hideStartDeg = 0f;
		[SerializeField] float showStartDeg = 0f;
		float currStartDeg = 0f;

		float currStartRad = 0f;
		[SerializeField] AnimationCurve angleCurve;


		[Header("Size")]
		[SerializeField] Vector2 showenSize;
		[SerializeField] Vector2 hideSize;
		Vector2 currSize;
		[SerializeField] AnimationCurve sizeCurve;


		[Header("Position")]
		[SerializeField] RectOffset padding;
		Vector2 realPosition; // half height and width
		Vector2 currPosition; // half height and width 
		Vector3 center;
		[SerializeField] AnimationCurve positionCurve;

		[Header("Element Rotation Type")]
		[SerializeField] CircleLayoutRotationType rotationType;
		[SerializeField] float rotatioDeg;

		[Header("Other")]
		[SerializeField] float animationTime = 1f;
		float deltaRad;
	 	float deltaDeg;
	 	#endregion

	 	#region override
		protected override void Awake ()
		{
			base.Awake ();
			Rebuild();
		}
		protected override void OnRectTransformDimensionsChange ()
		{
			base.OnRectTransformDimensionsChange ();
			CreateSizes();
			CurrTime = _currTime;
			SetPositions();
			SetAngles();
		}
		#endregion

		CircleLayoutState _state = CircleLayoutState.None;
		public CircleLayoutState CurrState
		{
			get { return _state; }
			set
			{
				_state = value;
				switch(value)
				{
					case CircleLayoutState.Showen: CurrTime = 1f; break;
					case CircleLayoutState.Hidden: CurrTime = 0f; break;
				}
			}
		}

		float _currTime = 0f;
		public float CurrTime
		{
			get { return _currTime; }
			private set
			{
				_currTime = value;
				currStartDeg = hideStartDeg + (showStartDeg - hideStartDeg) * angleCurve.Evaluate(_currTime);
				currSize = hideSize + (showenSize - hideSize) * sizeCurve.Evaluate(_currTime);
				currPosition = positionCurve.Evaluate(_currTime) * realPosition;
				currStartRad = Mathf.Deg2Rad * currStartDeg;
			}
		}

		public float AnimationTime
		{
			get { return animationTime; }
			set
			{
				if(value > 0)
				{
					animationTime = value;
				}
			}
		}

		[ContextMenu("RebuildAll")]
		public void Rebuild()
		{
			CreateList();
			CreateSizes();
			CurrState = startState;
			SetPositions();
			SetAngles();
		}

		void CreateList()
		{
			if(rect == null) rect = gameObject.GetComponent<RectTransform>();
			list.Clear();
			foreach(Transform child in transform)
			{
				list.Add((RectTransform)child.gameObject.GetComponent<RectTransform>());
			}
		}
		void CreateSizes()
		{
			if(rect == null) rect = gameObject.GetComponent<RectTransform>();
			realPosition = new Vector2(
				rect.rect.width - padding.left - padding.right,
				rect.rect.height - padding.bottom - padding.top) / 2f;
			center = new Vector3((padding.left - padding.right) / 2f, (padding.bottom - padding.top) / 2f, 0f);

			deltaRad = 2 * Mathf.PI / list.Count;
			deltaDeg = 360f / list.Count;

		}

		void SetPositions()
		{
			for(int i = 0; i < list.Count; i ++)
			{
				list[i].localPosition = center + (Vector3)GetPosition(currStartRad + deltaRad * i, currPosition.x, currPosition.y);
				list[i].sizeDelta = currSize;
			}
		}
		void SetAngles()
		{
			switch(rotationType)
			{
				case CircleLayoutRotationType.LocalRotation: 
					SetLocalRotation();
					break;
				case CircleLayoutRotationType.GlobalRotation: 
					SetGlobalRotation();
					break;
				case CircleLayoutRotationType.CenterRotation: 
					SetCenterRotation();
					break;
				case CircleLayoutRotationType.CenterRotationInvertedLocal: 
					SetCenterRotationInvertedLocal();
					break;
				case CircleLayoutRotationType.CenterRotationInvertedGlobal: 
					SetCenterRotationInvertedGlobal();
					break;
			}
		}

		Vector2 GetPosition(float angle, float a, float b)
		{
			return new Vector2(a * Mathf.Cos(angle),b * Mathf.Sin(angle));
		}

		void Update()
		{
			
			if(CurrState == CircleLayoutState.ShowenToHidden)
			{
				CurrTime = Mathf.Clamp01(CurrTime - Time.deltaTime / animationTime);
				SetPositions();
				SetAngles();
				if(CurrTime <= 0f)
				{
					CurrState = CircleLayoutState.Hidden;
				}
			}
			else if(CurrState == CircleLayoutState.HiddenToShowen)
			{
				CurrTime = Mathf.Clamp01(CurrTime + Time.deltaTime / animationTime);
				SetPositions();
				SetAngles();
				if(CurrTime >= 1f)
				{
					CurrState = CircleLayoutState.Showen;
				}
			}
			
		}

		#region setAngleRegion
		void SetLocalRotation()
		{
			for(int i = 0; i < list.Count; i ++)
			{
				list[i].localEulerAngles = new Vector3(0,0,rotatioDeg);
			}
		}
		void SetGlobalRotation()
		{
			for(int i = 0; i < list.Count; i ++)
			{
				list[i].eulerAngles = new Vector3(0,0,rotatioDeg);
			}
		}
		void SetCenterRotation()
		{
			float deg = currStartDeg + rotatioDeg;
			for(int i = 0; i < list.Count; i ++)
			{
				list[i].localEulerAngles = new Vector3(0, 0, deg + deltaDeg * i);
			}
		}
		void SetCenterRotationInvertedLocal()
		{
			float deg;
			for(int i = 0; i < list.Count; i ++)
			{
				deg = currStartDeg + deltaDeg * i + rotatioDeg;
				deg = deg.Loop(-90f, 90f);
				list[i].localEulerAngles = new Vector3(0,0,deg);
			}
		}
		void SetCenterRotationInvertedGlobal()
		{
			float deg;
			float min = -90f - transform.eulerAngles.z;
			float max = 90f - transform.eulerAngles.z;
			for(int i = 0; i < list.Count; i ++)
			{
				deg = currStartDeg + deltaDeg * i + rotatioDeg;
				deg = deg.Loop(min, max);
				list[i].localEulerAngles = new Vector3(0, 0, deg);
			}
		}
		#endregion
	}
}
