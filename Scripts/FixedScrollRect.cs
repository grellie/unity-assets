﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityEngine.UI.Components
{
	[AddComponentMenu("UI/Fixed Scroll Rect")]
	[SelectionBase]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class FixedScrollRect : ScrollRect
	{
		[Header("FixedRectParameters")]
		#region parameters
	    [Tooltip("Count of horizontal steps")]
	    [SerializeField]  int horizontalSteps = 1;
	    [Tooltip("Count of vertical steps")]
	    [SerializeField] int verticalSteps = 1;
	    [Tooltip("returning to position  speed")]
	    [SerializeField] float Speed = 0.1f;
		[SerializeField] float minHorizontalSpeed = 0f;

		#endregion

		public event Action<int, int> OnIntValueChanged; 

	    int currHorizontalStep = 0, currVerticalStep = 0;
		public int HorizontalSteps
		{
			get {return horizontalSteps;}
		}
		public int VerticalSteps
		{
			get {return verticalSteps;}
		}
		public int CurrHorizontalStep
		{
			get{return currHorizontalStep;}
			protected set
			{
				if(currHorizontalStep == value) return;
				currHorizontalStep = value;
				if(OnIntValueChanged != null) OnIntValueChanged(CurrHorizontalStep, CurrVerticalStep);
			}
		}
		public int CurrVerticalStep 
		{
			get{return currVerticalStep;}
			protected set
			{
				if(currVerticalStep == value) return;
				currVerticalStep = value;
				if(OnIntValueChanged != null) OnIntValueChanged(CurrHorizontalStep, CurrVerticalStep);
			}
		}
		
		public void SetPosition(int horizontal, int vertical)
		{
			if(horizontal < HorizontalSteps)
			{
				CurrHorizontalStep = horizontal;
				SetHorizontalPosition(CurrHorizontalStep / (HorizontalSteps - 1f), 1);
			}
			if(vertical < VerticalSteps)
			{
				CurrVerticalStep = vertical;
				SetVerticalPosition(CurrVerticalStep / (VerticalSteps - 1f), 1);
			}
		}
		

		#region override
		protected override void Awake ()
		{
			base.Awake ();
			StartCoroutine("GoToNearestHorizontalPosition");
			StartCoroutine("GoToNearestVerticalPosition");
		}
	    public override void OnEndDrag(PointerEventData eventData)
		{
	        base.OnEndDrag(eventData);
			StartCoroutine("GoToNearestHorizontalPosition");
	        StartCoroutine("GoToNearestVerticalPosition");
	        
	    }
	    public override void OnBeginDrag(PointerEventData eventData)
	    {
	        base.OnBeginDrag(eventData);
	        StopAllCoroutines();
	    }
	    #endregion

	    private IEnumerator GoToNearestHorizontalPosition()
		{
	        if (HorizontalSteps > 1)
	        {
				//find current next value
	            float radius = 0.5f/(HorizontalSteps - 1f);
	            float value = 1f;
				int currHorStep = CurrHorizontalStep;
	            for (int i = 0; i < HorizontalSteps; i++)
	            {
	                float nextStep = i/(HorizontalSteps - 1f);
	                if (Mathf.Abs(nextStep - horizontalNormalizedPosition) < radius)
	                {
	                    value = i/(HorizontalSteps - 1f);
	                    CurrHorizontalStep = i;
	                    break;
	                }
	            }
	            //check if we still in current position
				float prev = horizontalNormalizedPosition;
				yield return new WaitForFixedUpdate();
				float speed = horizontalNormalizedPosition - prev;
				if(currHorStep == CurrHorizontalStep)
				{
					if(speed > minHorizontalSpeed) CurrHorizontalStep = currHorStep + 1;
					else if ( speed < - minHorizontalSpeed)  CurrHorizontalStep = currHorStep - 1;
					else  CurrHorizontalStep = currHorStep;
					
					CurrHorizontalStep = Mathf.Clamp(CurrHorizontalStep, 0, HorizontalSteps - 1);
					 
					value = CurrHorizontalStep / (HorizontalSteps - 1f);
					
				}

	            //set to value
	            while (!SetHorizontalPosition(value, Speed/HorizontalSteps))
	            {
	                yield return new WaitForFixedUpdate();
	            }
	        }
	    }
	    private IEnumerator GoToNearestVerticalPosition()
		{
			float prev = verticalNormalizedPosition;
			yield return new WaitForFixedUpdate();
			while(Mathf.Abs( verticalNormalizedPosition - prev) > 0.005f)
			{
				prev = verticalNormalizedPosition;
				yield return new WaitForFixedUpdate();
			}
	       
	        if (VerticalSteps > 1)
	        {
	            float radius = 0.5f / (VerticalSteps - 1);
	            float value = 1f;
	            for (int i = 0; i < VerticalSteps; i++)
	            {
	                float nextStep = i / (VerticalSteps - 1f);
	                if (Mathf.Abs(nextStep - verticalNormalizedPosition) < radius)
	                {
	                    value = i / (VerticalSteps - 1f);
	                    CurrVerticalStep = i;
	                    break;
	                }
	            }
	            while (!SetVerticalPosition(value, Speed / VerticalSteps))
	            {
					yield return new WaitForFixedUpdate();
	            }
	        }
	    }
	    private bool SetHorizontalPosition(float value, float step)
	    {
	        if (horizontalNormalizedPosition > value)
	        {
	            horizontalNormalizedPosition -= step;
	            if (horizontalNormalizedPosition < value)
	            {
	                horizontalNormalizedPosition = value;
	                return true;
	            }
	        }
	        else
	        {
	            horizontalNormalizedPosition += step;
	            if (horizontalNormalizedPosition > value)
	            {
	                horizontalNormalizedPosition = value;
	                return true;
	            }
	        }
	        return false;
	    }
	    private bool SetVerticalPosition(float value, float step)
	    {
	        if (verticalNormalizedPosition > value)
	        {
	            verticalNormalizedPosition -= step;
	            if (verticalNormalizedPosition < value)
	            {
	                verticalNormalizedPosition = value;
	                return true;
	            }
	        }
	        else
	        {
	            verticalNormalizedPosition += step;
	            if (verticalNormalizedPosition > value)
	            {
	                verticalNormalizedPosition = value;
	                return true;
	            }
	        }
	        return false;
	    }
	}
}
