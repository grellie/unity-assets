﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace UnityEngine.UI.Components
{
	[CustomEditor(typeof(BubbleLayoutGroup))]
	public class BubbleLayoutGroupEditor : Editor 
	{

		public override void OnInspectorGUI ()
		{
			BubbleLayoutGroup  Target = (BubbleLayoutGroup)target;
			base.OnInspectorGUI ();
			GUILayout.BeginHorizontal();
			if(GUILayout.Button("Rebuild"))
			{
				Target.Rebuild();
			}

			GUILayout.EndHorizontal();
			EditorGUILayout.HelpBox("Mind the child count", MessageType.Warning);
		}
	}
}