﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

namespace UnityEngine.UI.Components
{
	[CustomEditor(typeof(CircleLayoutGroup))]
	public class CircleLayoutGroupEditor : Editor 
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI ();
			CircleLayoutGroup Target = (CircleLayoutGroup)target;
			if(GUILayout.Button("Rebuild"))
			{
				Target.Rebuild();
			}
			if(GUILayout.Button("Show"))
			{
				Target.CurrState = CircleLayoutState.HiddenToShowen;
			}
			if(GUILayout.Button("Hide"))
			{
				Target.CurrState = CircleLayoutState.ShowenToHidden;
			}
		}
	}
}
