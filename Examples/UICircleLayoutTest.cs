﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI.Components;

public class UICircleLayoutTest : MonoBehaviour 
{
	[SerializeField] CircleLayoutGroup group;

	public void PressHide()
	{
		group.CurrState = CircleLayoutState.ShowenToHidden;
	}
	public void PressShow()
	{
		group.CurrState = CircleLayoutState.HiddenToShowen;
	}
	public void SpeedChanged(UnityEngine.UI.Slider slider)
	{
		group.AnimationTime = slider.value;
	}
}
